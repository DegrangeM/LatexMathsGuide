# LatexMathsGuide

LatexMathsGuide est une collection d'outils permettant de faciliter la génération de code LaTeX liés aux mathématiques.

Ce logiciel est libre (licence MIT) et gratuit.

## Accès aux outils

Pour accéder à la liste des outils disponibles, utilisez ce lien : https://degrangem.forge.apps.education.fr/LatexMathsGuide/

## Documentation

Lien vers la documentation : https://forge.apps.education.fr/DegrangeM/LatexMathsGuide/-/wikis/home

## Capture d'écran
![image](/uploads/8654d0abaf7e5c36cae5193b974664bf/image.png)