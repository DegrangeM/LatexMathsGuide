import{a as f,t as c,s as a,f as h,c as p}from"../chunks/disclose-version.CNqpm4Un.js";import{p as x,a as _}from"../chunks/runtime.asT1dh8U.js";import{a as b}from"../chunks/render.D1h3o7Y5.js";import{s as d}from"../chunks/attributes.S74sUJI1.js";import{u as k,i as M,s as y,p as $}from"../chunks/stores.DVXDYRE1.js";function w(t){var r;var e=(r=t.$$slots)==null?void 0:r.default;return e===!0?t.children:e}const D=!0,C=Object.freeze(Object.defineProperty({__proto__:null,prerender:D},Symbol.toStringTag,{value:"Module"}));var L=c(`<div style="
    position: absolute;
    top: 15px;
    left: 5px;
    transform: rotate(-45deg);
    font-size: 42px;
    text-shadow: 0px 2px 2px black;
    color: cornflowerblue;
    font-family: impact;
    pointer-events: none;
    opacity: 0.8;
    z-index:10;
">Bêta</div>`);function z(t){var e=L();f(t,e)}var G=c('<!> <!> <div class="uk-flex uk-flex-between uk-margin"><div class="uk-text-italic uk-margin-left"><a>Retour à la liste des outils</a> - Un outil réalisé par Mathieu Degrange - <a>Mentions légales</a></div> <div class="uk-text-italic uk-margin-right"><a href="https://forge.apps.education.fr/DegrangeM/LatexMathsGuide">Code source</a> disponible sur la forge - <a href="https://forge.apps.education.fr/DegrangeM/LatexMathsGuide/-/wikis/">Documentation</a></div></div>',1);function P(t,e){x(e,!1);const r={};k(r);const o=()=>y($,"$page",r);let s=o().url.hostname==="localhost"?o().url.origin:"https://degrangem.forge.apps.education.fr/LatexMathsGuide";M();var i=G(),n=h(i);z(n);var l=a(a(n,!0));b(l,w(e),{});var g=a(a(l,!0)),v=p(g),u=p(v);d(u,"href",`${s??""}/`);var m=a(a(u,!0));d(m,"href",`${s??""}/Mentions-Legales/`),f(t,i),_()}export{P as component,C as universal};
