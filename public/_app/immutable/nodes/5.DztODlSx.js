import{a as v,t as h,c as _,s as n,f as b}from"../chunks/disclose-version.CNqpm4Un.js";import{g as a,b as i,s as k,d as u}from"../chunks/runtime.asT1dh8U.js";import{b as y,p as m}from"../chunks/index-client.DfTcfyOw.js";import{M as x}from"../chunks/MathField.CUSm8XgA.js";import{S as M,D as w}from"../chunks/DataManager.DZP3xS-B.js";var D=h('<div class="uk-card uk-card-body uk-card-default uk-text-center" style="width:100%;"><!></div> <!> <!>',1);function F(l){let e=k(""),r={},g=u(()=>`\\documentclass{standalone}
\\usepackage{amsmath}
\\begin{document}
\\large
$\\displaystyle`+a(e)+`$
\\end{document}`);var o=D(),s=b(o),c=_(s);x(c,{get onchange(){return r.generer_image},get value(){return a(e)},set value(t){i(e,m(t))},width:"auto",minWidth:100});var d=n(n(s,!0));y(M(d,{get LaTeX(){return a(g)},get code(){return a(e)}}),t=>r=t,()=>r);var p=n(n(d,!0)),f=u(()=>({formule:a(e)}));w(p,{get data(){return a(f)},onload:t=>{i(e,m(t.formule)),r.generer_image()}}),v(l,o)}export{F as component};
