export function getKrokiUrl(LaTeX) {
    // Lorsque l'on écrit une fraction par exemple, le clavier MathLive ajoute un placeholder
    // Il faut l'enlever pour que LaTeX puisse compiler correctement
    // Par mesure de simplicité, on ne le corrige quand dans la sortie image
    // Le code LaTeX reste inchangé
    LaTeX = LaTeX.replaceAll("\\placeholder{}", "{}");

    var data = new TextEncoder("utf-8").encode(LaTeX);
    var compressed = pako.deflate(data, { level: 9, to: "string" });
    var result = btoa(compressed)
        .replace(/\+/g, "-")
        .replace(/\//g, "_");

    return result;
}